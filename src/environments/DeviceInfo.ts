import React, { useEffect, useState } from "react";
import FingerprintJS from "@fingerprintjs/fingerprintjs";
import CryptoJS from "crypto-js";
 
// Generate base-62 characters dynamically
const characters = [
  ...Array.from(Array(10), (_, i) => String.fromCharCode(48 + i)), // '0'-'9'
  ...Array.from(Array(26), (_, i) => String.fromCharCode(97 + i)), // 'a'-'z'
  ...Array.from(Array(26), (_, i) => String.fromCharCode(65 + i)), // 'A'-'Z'
].join('');
 
// Function to convert a hex string to a base-62 string
const base62Encode = (hex: any) => {
  let base62 = '';
  let number = BigInt('0x' + hex);
 
  while (number > 0) {
    const remainder = number % 62n;
    base62 = characters[Number(remainder)] + base62;
    number = number / 62n;
  }
 
  return base62;
};
 
export const DeviceInfo = () => {
  const [DEVICE_ID, setDeviceId] = useState("");
 
  useEffect(() => {
    const fpPromise = FingerprintJS.load();
 
    const fetchDeviceId = async () => {
      try {
        const fp = await fpPromise;
        const result = await fp.get();
        const visitorId = result.visitorId;  
        const hash = CryptoJS.SHA256(visitorId).toString(CryptoJS.enc.Hex);
        const shortDeviceId = base62Encode(hash).substring(0, 8); // Use 8 characters for more uniqueness     
        setDeviceId(shortDeviceId);
      } catch (error) {
        console.error("Error obtaining fingerprint:", error);
        setDeviceId("Unavailable");
      }
    };
 
    fetchDeviceId();
  }, []);
 
  return DEVICE_ID;
};