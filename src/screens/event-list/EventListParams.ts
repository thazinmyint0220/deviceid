import { User } from "../../models/User";

export class EventListParams {
  private _user: User = new User();

  get user(): User {
    return this._user.clone();
  }

  set user(value: User) {
    this._user = value;
  }
}
