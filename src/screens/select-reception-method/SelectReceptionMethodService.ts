import { executeQuery } from "../../aws/db/dbOperation";

//get event name, to show as title
export const fetchEventNameById = async (eventid: number) => {
  const method = "POST";
  const queryString = "SELECT name FROM event WHERE event_id =" + eventid + ";";

  return executeQuery(method, queryString);
};
